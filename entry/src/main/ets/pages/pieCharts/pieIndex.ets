/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { PieChart } from '@ohos/mpchart'
import { PieData } from '@ohos/mpchart';
import { PieDataSet, ValuePosition } from '@ohos/mpchart';
import { PieEntry } from '@ohos/mpchart';
import { JArrayList } from '@ohos/mpchart';
import { ColorTemplate } from '@ohos/mpchart'
import { MPPointF } from '@ohos/mpchart';
import { SeekBar } from '@ohos/mpchart';
import {
  Legend,
  LegendForm,
  LegendVerticalAlignment,
  LegendOrientation,
  LegendHorizontalAlignment
} from '@ohos/mpchart';
import { ImagePaint } from '@ohos/mpchart'
import title from '../title/index';

@Entry
@Component
struct pieIndex {
  pieData: PieData | null = null;
  @State pieModel: PieChart.Model = new PieChart.Model()
  @State @Watch("seekBarXValueWatch") seekBarX: SeekBar.Model = new SeekBar.Model()
  @State @Watch("seekBarYValueWatch") seekBarY: SeekBar.Model = new SeekBar.Model()
  parties: string[] = [
    "Party A", "Party B", "Party C", "Party D", "Party E", "Party F", "Party G", "Party H",
    "Party I", "Party J", "Party K", "Party L", "Party M", "Party N", "Party O", "Party P",
    "Party Q", "Party R", "Party S", "Party T", "Party U", "Party V", "Party W", "Party X",
    "Party Y", "Party Z"]
  //  private menuItemArr: Array<string> = ['View on GitHub', 'Toggle Y-Values', 'Toggle X-Values', 'Toggle Icons', 'Toggle Percent', 'Toggle Minimum Angles',
  //  'Toggle Hole', 'Toggle Curved Slices', 'Draw Center Text', 'Spin Animation', 'Animate X', 'Animate Y', 'Animate XY', 'Save to Gallery']

  private menuItemArr: Array<string> = ['Toggle Y-Values', 'Toggle X-Values', 'Toggle Icons', 'Toggle Percent', /*'Toggle Minimum Angles',*/
    'Toggle Hole', 'Toggle Curved Slices', 'Draw Center Text', 'Spin Animation', 'Animate X', 'Animate Y', 'Animate XY']
  private title: string = 'PieChartActivity'
  @State @Watch("menuCallback") model: title.Model = new title.Model()

  aboutToAppear(): void {
    this.pieData = this.initPieData(4, 10);
    this.pieModel
      .setPieData(this.pieData)
      .setRadius(150)
      .setHoleRadius(0.5)
      .setOffset(new MPPointF(160, 200))// vp
      .setUsePercentValues(true)

    this.animate('Animate XY')

    this.seekBarX.setValue(4)
      .setMax(25)
      .setMin(0)

    this.seekBarY.setValue(10)
      .setMax(200)
      .setMin(0)

    let l: Legend = this.pieModel.getLegend()
    l.setOrientation(LegendOrientation.VERTICAL)
    l.setVerticalAlignment(LegendVerticalAlignment.TOP);
    l.setHorizontalAlignment(LegendHorizontalAlignment.RIGHT);
    l.setYEntrySpace(10)
    l.setFormToTextSpace(20)

    this.model.menuItemArr = this.menuItemArr
    this.model.title = this.title
  }

  menuCallback() {
    if (this.model == null || this.model == undefined) {
      return
    }
    let index: number = this.model.getIndex()

    switch (index) {
      case 0:
      //prompt.showToast({ message: 'Entry added!', duration: 1000});
        this.pieModel.setUsePercentValues(!this.pieModel.isUsePercentValuesEnabled())
        this.pieModel.init()
        break;
      case 1:
        this.pieModel.setDrawSliceText(!this.pieModel.isDrawEntryLabelsEnabled())
        this.pieModel.init()
        break;
      case 2:
        this.pieModel.getPieData()
          .getDataSet()
          .setDrawIcons(!this.pieModel.getPieData().getDataSet().isDrawIconsEnabled())
        this.pieModel.init()
        break;
      case 3:
        this.pieModel.setUsePercentValues(!this.pieModel.isUsePercentValuesEnabled())
        this.pieModel.init()
        break;
    //      case 4:
    //        if (this.pieModel.getMinAngleForSlices() == 0) {
    //          this.pieModel.setMinAngleForSlices(36)
    //        }
    //        else {
    //          this.pieModel.setMinAngleForSlices(0)
    //        }
    //        this.pieModel.init()
    //        break;
      case 4:
        this.pieModel.setDrawHoleEnabled(!this.pieModel.isDrawHoleEnabled())
        this.pieModel.init()
        break;
      case 5:
        this.pieModel.setDrawRoundedSlices(!this.pieModel.isDrawRoundedSlicesEnabled())
        this.pieModel.init()
        break;
      case 6:
        this.pieModel.setDrawCenterText(!this.pieModel.isDrawCenterTextEnabled())
        this.pieModel.init()
        break;
      case 7:
        this.spinAnimation()
        break;
      case 8:
        this.animate('Animate X')
        break;
      case 9:
        this.animate('Animate Y')
        break;
      case 10:
        this.animate('Animate XY')
        break;
    }
    this.model.setIndex(-1)
  }

  public animate(animate: string) {
    let phase = 1
    let that = this
    let intervalID = setInterval( () => {
      if (animate == 'Animate X') {
        that.pieModel.mAnimator.setPhaseX(phase / 10)
      } else if (animate == 'Animate Y') {
        that.pieModel.mAnimator.setPhaseY(phase / 10)
      } else {
        that.pieModel.mAnimator.setPhaseX(phase / 10)
        that.pieModel.mAnimator.setPhaseY(phase / 10)
      }
      that.pieModel.init()
      phase += 1
      if (phase > 10) {
        clearInterval(intervalID);
      }
    }, 100);
  }

  public spinAnimation() {
    let startAngle = 10
    let that = this
    let intervalID = setInterval( () => {
      that.pieModel.setRotationAngle(that.pieModel.mRotationAngle + 10).init()
      startAngle += 10
      if (startAngle > 360) {
        clearInterval(intervalID);
      }
    }, 30);
  }

  // 初始化饼状图数据
  private initPieData(count: number, range: number): PieData {
    let entries = new JArrayList<PieEntry>();
    for (let i = 0; i < count; i++) {
      let paint: ImagePaint = new ImagePaint()
      paint.setIcon($r('app.media.star'))
      entries.add(new PieEntry(((Math.random() * range) + range / 5), this.parties[i % this.parties.length], paint))
    }
    //    entries.add(new PieEntry(7, 'Party A', paint))
    //    entries.add(new PieEntry(2, 'Party B', paint))
    //    entries.add(new PieEntry(5, 'Party C', paint))
    //    entries.add(new PieEntry(3, 'Party D', paint))

    let dataSet: PieDataSet = new PieDataSet(entries, "Election Results");
    dataSet.setDrawIcons(false);
    dataSet.setSliceSpace(3);
    dataSet.setIconsOffset(new MPPointF(0, 40));
    dataSet.setSelectionShift(5);

    // add a lot of colors
    let colors: JArrayList<number> = new JArrayList();
    for (let index = 0; index < ColorTemplate.VORDIPLOM_COLORS.length; index++) {
      colors.add(ColorTemplate.VORDIPLOM_COLORS[index]);
    }

    for (let index = 0; index < ColorTemplate.JOYFUL_COLORS.length; index++) {
      colors.add(ColorTemplate.JOYFUL_COLORS[index]);
    }

    for (let index = 0; index < ColorTemplate.COLORFUL_COLORS.length; index++) {
      colors.add(ColorTemplate.COLORFUL_COLORS[index]);
    }
    for (let index = 0; index < ColorTemplate.LIBERTY_COLORS.length; index++) {
      colors.add(ColorTemplate.LIBERTY_COLORS[index]);
    }
    for (let index = 0; index < ColorTemplate.PASTEL_COLORS.length; index++) {
      colors.add(ColorTemplate.PASTEL_COLORS[index]);
    }
    colors.add(ColorTemplate.getHoloBlue());
    dataSet.setColorsByList(colors);


    return new PieData(dataSet)
  }

  private changeData() {
    this.pieData = this.initPieData(4, 10);
    this.pieModel.setPieData(this.pieData)
    this.pieModel.invalidate();
  }

  build() {
    Column() {
      title({ model: this.model })
      Stack({ alignContent: Alignment.TopStart }) {
        PieChart({
          model: $pieModel
        })
        //        Column() {
        //          SeekBar({ model: this.seekBarX })
        //          SeekBar({ model: this.seekBarY })
        //        }
      }
      Button("切换数据").onClick(() => {
        this.changeData();
      }).margin(30).position({x:'0%', y:'60%'})
    }
  }

  seekBarXValueWatch(): void {
    this.pieModel.setPieData(this.initPieData(this.seekBarX.getValue(), this.seekBarY.getValue()));
    this.pieModel.init()
  }

  seekBarYValueWatch(): void {
    this.pieModel.setPieData(this.initPieData(this.seekBarX.getValue(), this.seekBarY.getValue()));
    this.pieModel.init()
  }
}