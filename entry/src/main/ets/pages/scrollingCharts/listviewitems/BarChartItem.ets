/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { BarData } from '@ohos/mpchart'
import { LegendEntry } from '@ohos/mpchart'
import {BarChart,BarChartModel} from '@ohos/mpchart'
import { XAxis,XAxisPosition } from '@ohos/mpchart'
import {YAxis,AxisDependency,YAxisLabelPosition} from '@ohos/mpchart'
import { MultipleLegend } from '@ohos/mpchart'

@Entry
@Preview
@Component
export default struct BarChartItem {
  data: BarData | null = null;

  @State barModel: BarChartModel = new BarChartModel()
  mChartData: BarData | null = null;
  mWidth: number = 350; //表的宽度
  mHeight: number = 300; //表的高度
  minOffset: number = 15; //X轴线偏移量
  leftAxis: YAxis | null = null;
  rightAxis: YAxis | null = null;
  bottomAxis: XAxis = new XAxis();
  legendArr:LegendEntry[]=[]


  aboutToAppear(){
    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(7, false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setSpaceTop(15);
    this.leftAxis.setAxisMinimum(0);
    this.leftAxis.setAxisMaximum(100);
    this.leftAxis.enableGridDashedLine(10,10,0)

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(false);
    this.rightAxis.setLabelCount(7, false);
    this.rightAxis.setSpaceTop(11);
    this.rightAxis.setAxisMinimum(0); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(10);
    this.rightAxis.setDrawAxisLine(false);
    this.rightAxis.setDrawLabels(false);

    this.bottomAxis.setLabelCount(5, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(0);
    this.bottomAxis.setAxisMaximum(12);

    if(this.data){
      this.barModel.setData(this.data!)
    }
    this.barModel.mWidth = this.mWidth;
    this.barModel.mHeight = this.mHeight;
    this.barModel.init();
    this.barModel.setDrawBarShadow(false);
    this.barModel.setDrawValueAboveBar(true);
    this.barModel.getDescription().setEnabled(false);
    this.barModel.setMaxVisibleValueCount(60);
    this.barModel.setLeftYAxis(this.leftAxis);
    this.barModel.setRightYAxis(this.rightAxis);
    this.barModel.setXAxis(this.bottomAxis)
    this.barModel.mRenderer.initBuffers();
    this.barModel.prepareMatrixValuePx();
    this.setLegend();
  }


  public setLegend(){

    for(let i=0; i<this.barModel.getBarData().getDataSets().size(); i++) {
      let dataSet = this.barModel.getBarData().getDataSetByIndex(i)
      let legendLine:LegendEntry =new LegendEntry();
      legendLine.colorHeight=10
      legendLine.colorHeight=10
      legendLine.colorItemSpace=3
      legendLine.colorLabelSpace=4
      legendLine.labelColor=Color.Black
      legendLine.labelTextSize=10
      if (dataSet) {
        legendLine.colorArr=dataSet.getColors().dataSource
        legendLine.label=dataSet.getLabel()
      }
      this.legendArr.push(legendLine)
    }
  }

  build() {
    Column(){
    Stack(){
      BarChart({model:this.barModel})
    }
    Row() {
      ForEach(this.legendArr.map((data, index) => {
        let value:valueItem = { index: index, legendItem: data }
        return value;
      }), (item: valueItem) => {
        Blank().width(4)
        MultipleLegend({legend:item.legendItem});
      }, (item: valueItem) => item.index+"")
    }.margin({left:50})
  }.alignItems(HorizontalAlign.Start)
  }
}
interface valueItem{
  index: number,
  legendItem: LegendEntry
}

