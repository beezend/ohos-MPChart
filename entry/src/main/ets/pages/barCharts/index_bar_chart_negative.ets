/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ColorTemplate } from '@ohos/mpchart';
import { XAxis,XAxisPosition } from '@ohos/mpchart'
import {YAxis,AxisDependency,YAxisLabelPosition} from '@ohos/mpchart'
import { BarEntry } from '@ohos/mpchart';
import { JArrayList } from '@ohos/mpchart';
import { BarDataSet } from '@ohos/mpchart';
import { BarData } from '@ohos/mpchart';
import {BarChart,BarChartModel} from '@ohos/mpchart'
import { IBarDataSet } from '@ohos/mpchart'
import { AxisBase } from '@ohos/mpchart'
import { IAxisValueFormatter } from '@ohos/mpchart'
import title from '../title/index';

class MyAxisValueFormatter implements IAxisValueFormatter {
  mActivities:string[] = ["12-29", "12-29", "12-30", "12-31", "01-01","01-02"];
  public getFormattedValue(value: number, axis: AxisBase): string {
    return this.mActivities[value % this.mActivities.length];
  }
}

@Entry
@Component
struct Index_bar_chart_Negative {

  @State model:BarChartModel = new BarChartModel();
  mWidth: number = 350; //表的宽度
  mHeight: number = 300; //表的高度
  minOffset: number = 15; //X轴线偏移量
  leftAxis: YAxis | null = null;
  rightAxis: YAxis | null = null;
  bottomAxis: XAxis = new XAxis();//标题栏菜单文本
  private menuItemArr: Array<string> = [];
  //标题栏标题
  private title: string = 'BarChart'
  @State @Watch("menuCallback") titleModel: title.Model = new title.Model()
  //标题栏菜单回调
  menuCallback(){
    if (this.titleModel == null || this.titleModel == undefined) {
      return
    }
    let index: number = this.titleModel.getIndex()
    if(index==undefined||index==-1){
      return
    }
    this.titleModel.setIndex(-1)
  }

  public aboutToAppear(){
    this.titleModel.menuItemArr = this.menuItemArr
    this.titleModel.title = this.title
    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(6, false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setAxisMinimum(-3200);
    this.leftAxis.setAxisMaximum(2200);

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(false);
    this.rightAxis.setLabelCount(6, false);
    this.rightAxis.setAxisMinimum(-3200); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(2200);

    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(0);
    this.bottomAxis.setAxisMaximum(5);
    const mActivities:string[] = ["12-29", "12-29", "12-30", "12-31", "01-01","01-02"];
    this.bottomAxis.setValueFormatter(new MyAxisValueFormatter());
    this.setData(this.bottomAxis.getAxisMaximum(),this.leftAxis.getAxisMaximum())

    this.model.mWidth = this.mWidth;
    this.model.mHeight = this.mHeight;
    this.model.setLeftYAxis(this.leftAxis);
    this.model.setRightYAxis(this.rightAxis);
    this.model.setXAxis(this.bottomAxis)
    this.model.init();
    this.model.setDrawBarShadow(false);
    this.model.setDrawValueAboveBar(true);
    this.model.setMaxVisibleValueCount(60);
    this.model.mRenderer.initBuffers();
    this.model.prepareMatrixValuePx();
    this.model.mDrawClickText = false;

  }

  build() {
    Column(){
      title({ model: this.titleModel })
      Stack({ alignContent: Alignment.TopStart }){
        BarChart({model:this.model,visibilityAxis:Visibility.Hidden})
      }
    }.alignItems(HorizontalAlign.Start)
  }

  private setData(count: number, range: number) {

    let values:JArrayList<BarEntry> = new JArrayList<BarEntry>();
    let valueColors:JArrayList<number> = new JArrayList<number>();

    values.add(new BarEntry(0, -224.1));
    values.add(new BarEntry(1, 238.5));
    values.add(new BarEntry(2, 1280.1));
    values.add(new BarEntry(3, -442.3));
    values.add(new BarEntry(4, -2280.1));

    for(let i = 0; i < values.length(); i++){
      if(values.at(i).getY() > 0){
        valueColors.add(0xD34A58);
      } else {
        valueColors.add(0x6EBE66);
      }
    }

    let set1: BarDataSet;

    if (this.model.getBarData() != null &&
    this.model.getBarData().getDataSetCount() > 0) {
      set1 = this.model.getBarData().getDataSetByIndex(0) as BarDataSet;
      set1.setValues(values);
      this.model.getBarData().notifyDataChanged();
      this.model.notifyDataSetChanged();

    } else {
      set1 = new BarDataSet(values, "Data Set");

      set1.setValueTextColors(valueColors);
      set1.setColorsByVariable(valueColors.toArray());
      set1.setDrawValues(true);

      let dataSets: JArrayList<IBarDataSet> = new JArrayList<IBarDataSet>();
      dataSets.add(set1);

      let data: BarData = new BarData(dataSets);

      // data.setBarWidth(0);
      this.model.setData(data);
      this.model.setFitBars(true);
    }
  }
}