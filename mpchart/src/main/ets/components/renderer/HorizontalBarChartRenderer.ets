/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import YAxis,{AxisDependency} from '../components/YAxis';
import { HorizontalBarChartModel }  from '../charts/HorizontalBarChart';
import IBarDataSet from '../interfaces/datasets/IBarDataSet';
import Range from '../highlight/Range';
import BarLineScatterCandleBubbleRenderer from './BarLineScatterCandleBubbleRenderer';
import MyRect from '../data/Rect';
import BarBuffer from '../buffer/BarBuffer';
import Paint,{Style,RectPaint, ImagePaint, TextPaint} from '../data/Paint';
import BarData from '../data/BarData';
import BarEntry from '../data/BarEntry';
import Transformer from '../utils/Transformer';
import {MyDirection} from '../utils/Fill';
import MPPointF from '../utils/MPPointF';
import ViewPortHandler from '../utils/ViewPortHandler';
import Utils from '../utils/Utils';
import {JArrayList} from '../utils/JArrayList';
import Highlight from '../highlight/Highlight';
import ChartAnimator from '../animation/ChartAnimator'


/**
 * Renderer for the HorizontalBarChart.
 *
 * @author Philipp Jahoda
 */
export default class HorizontalBarChartRenderer extends BarLineScatterCandleBubbleRenderer {

  protected mChart:HorizontalBarChartModel | null = null;

  /**
     * the rect object that is used for drawing the bars
     */
  protected mBarRect:MyRect = new MyRect();

  protected mBarBuffers:BarBuffer[] |null = null;

  protected mShadowPaint:Paint =new RectPaint();
  protected mBarBorderPaint:Paint =new RectPaint();
  maxTextOffset:number = 0;
  private width:number = 0;
  private height:number = 0;
  private singleHeight:number = 0;
  public marginCount:number = 12;
  private marginBottom:number = 0;
  private count :number= 0;

  constructor(chart:HorizontalBarChartModel,viewPortHandler:ViewPortHandler) {
    super(null, viewPortHandler);
    this.mChart = chart;

    this.mHighlightPaint = new Paint();
    this.mHighlightPaint.setStyle(Style.FILL);
    this.mHighlightPaint.setColor(Color.Black);
    // set alpha after color
    this.mHighlightPaint.setAlpha(120);

    this.mShadowPaint.setStyle(Style.FILL);

    this.mBarBorderPaint.setStyle(Style.STROKE);
  }

  public initBuffers():void {
    if (this.mChart) {
      let barData:BarData | null = this.mChart.getBarData();
      if (barData) {
        this.mBarBuffers = new Array(barData.getDataSetCount());
        for (let i = 0; i < this.mBarBuffers.length; i++) {
          let dataSet:IBarDataSet | null= barData.getDataSetByIndex(i);
          if (dataSet) {
            this.mBarBuffers[i] = new BarBuffer(dataSet.getEntryCount() * 4 * (dataSet.isStacked() ? dataSet.getStackSize() : 1),
              barData.getDataSetCount(), dataSet.isStacked());
          }
        }
      }
    }
    this.mAnimator = new ChartAnimator();
  }

  public drawData():Paint[] {
    if (!this.mChart) {
      return []
    }
    this.singleHeight = 0;
    this.marginBottom = 0;
    this.count = 0;
    this.width = 0;
    this.height = 0;
    this.maxTextOffset = 0;

    let rectPaintArr:Paint[]=new Array();
    let barData:BarData | null= this.mChart.getBarData();
    if (!barData) {
      return [];
    }
    let leftAxis:YAxis | null = this.mChart.getAxis(AxisDependency.LEFT);
    if (leftAxis) {
      let textPaint:TextPaint = new TextPaint();
      textPaint.setTextSize(leftAxis.getTextSize())
      this.maxTextOffset = Utils.calcTextWidth(textPaint,leftAxis.getAxisMinimum() < 0?leftAxis.getAxisMinimum().toFixed(0)+"":leftAxis.getAxisMaximum().toFixed(0)+"");
    }
    if (this.mViewPortHandler) {
      let right = this.mViewPortHandler.contentRight() - this.maxTextOffset;
      this.width = right - this.mViewPortHandler.contentLeft() - this.maxTextOffset;
      this.height = this.mViewPortHandler.contentBottom() - this.mViewPortHandler.contentTop();
      for(let i = 0; i < barData.getDataSetCount(); i++){
        let dataSet:IBarDataSet | null= barData.getDataSetByIndex(i);
        if (dataSet) {
          let entries = dataSet.getEntries();
          if (entries) {
            this.count += entries.size();
          }
        }
      }
    }


    for (let i = 0; i < barData.getDataSetCount(); i++) {

      let dataSet: IBarDataSet | null = barData.getDataSetByIndex(i);
      if (dataSet) {
        let entries = dataSet.getEntries();
        if (entries) {
          this.count = entries.size();
        }
        this.singleHeight = this.height / this.count;
        this.marginBottom = this.marginCount / this.count;
        if (dataSet.isVisible()) {
          rectPaintArr = rectPaintArr.concat(this.drawDataSet(dataSet, i));
        }
      }
    }
    return  rectPaintArr;
  }

  private mBarShadowRectBuffer:MyRect = new MyRect();

  protected drawDataSet(dataSet:IBarDataSet,index:number):Paint[] {
    if (!this.mChart) {
      return []
    }
    let axisDependency = AxisDependency.LEFT;
    if (dataSet.getAxisDependency()) {
      axisDependency = dataSet.getAxisDependency();
    }
    let trans:Transformer | null = this.mChart.getTransformer(axisDependency);

    this.mBarBorderPaint.setColor(dataSet.getBarBorderColor());
    this.mBarBorderPaint.setStrokeWidth(Utils.convertDpToPixel(dataSet.getBarBorderWidth()));

    let drawBorder:boolean = dataSet.getBarBorderWidth() > 0;

    let phaseX:number = this.mAnimator.getPhaseX();
    let phaseY:number = this.mAnimator.getPhaseY();

    let rectPaintArr:Paint[]=new Array();
    // draw the bar shadow before the values
    if (this.mChart.isDrawBarShadowEnabled()) {
      this.mShadowPaint.setColor(dataSet.getBarShadowColor());

      let barData:BarData | null = this.mChart.getBarData();
      if (!barData) {
        return [];
      }
      let barWidth:number= barData.getBarWidth();
      let barWidthHalf:number = barWidth / 2.0;
      let x:number;
      for (let i = 0, count = Math.min((Math.ceil((dataSet.getEntryCount()) * phaseX)), dataSet.getEntryCount());
           i < count;
           i++) {

        let e = dataSet.getEntryForIndex(i);
        if (e) {
          x = e.getX();

          this.mBarShadowRectBuffer.left = x - barWidthHalf;
          this.mBarShadowRectBuffer.right = x + barWidthHalf;
        }

        if (trans) {
          trans.rectValueToPixel(this.mBarShadowRectBuffer);
        }

        if (!this.mViewPortHandler) {
          break;
        }
        if (!this.mViewPortHandler.isInBoundsLeft(this.mBarShadowRectBuffer.right))
        continue;

        if (!this.mViewPortHandler.isInBoundsRight(this.mBarShadowRectBuffer.left))
        break;

        this.mBarShadowRectBuffer.top = this.mViewPortHandler.contentTop();
        this.mBarShadowRectBuffer.bottom = this.mViewPortHandler.contentBottom();

        let rectPaint:RectPaint=new RectPaint(this.mShadowPaint as RectPaint);
        let style =this.mShadowPaint.getStyle();
        if (style) {
          rectPaint.setStyle(style)
        }
        rectPaint.setX(this.mBarShadowRectBuffer.left)
        rectPaint.setY(this.mBarShadowRectBuffer.top)
        rectPaint.setWidth(Math.abs(this.mBarShadowRectBuffer.right-this.mBarShadowRectBuffer.left))
        rectPaint.setHeight(Math.abs(this.mBarShadowRectBuffer.bottom-this.mBarShadowRectBuffer.top))
        rectPaintArr.push(rectPaint)
      }
    }

    // initialize the buffer
    if (!this.mBarBuffers) {
      return [];
    }
    let buffer:BarBuffer = this.mBarBuffers[index];
    buffer.setPhases(phaseX, phaseY);
    buffer.setDataSet(index);
    buffer.setInverted(this.mChart.isInverted(axisDependency));
    buffer.setBarWidth(this.mChart.getBarData()!.getBarWidth());
    buffer.feed(dataSet);
    if (trans) {
      trans.pointValuesToPixel(buffer.buffer);
    }

    let isCustomFill:boolean = false;
    let dataFills = dataSet.getFills();
    if (dataFills) {
      isCustomFill = dataFills && !dataFills.isEmpty();
    }

    let isSingleColor:boolean = dataSet.getColors().size() == 1;
    let isInverted:boolean = this.mChart.isInverted(axisDependency);
    if (isSingleColor) {
      this.mRenderPaint.setColor(dataSet.getColor());
    }
    let position:number = 0;
    let number = 0;
    for(let i = 0; i < index; i++){
      let barData = this.mChart.getBarData();
      if (!barData) {
        break;
      }
      let dataSet:IBarDataSet | null= barData.getDataSetByIndex(index - 1);

      if (dataSet) {
        let entries = dataSet.getEntries();
        if (entries) {
          number += entries.size();
        }
      }
    }
    for (let j = 0, pos = 0; j < buffer.size(); j += 4, pos++) {
      if (!dataSet){
        break;
      }
      if(position==dataSet.getEntryCount()){
        continue
      }
      let entries = dataSet.getEntries();
      if (!entries) {
        break;
      }
      let barEntry:BarEntry = entries.get(position) as BarEntry;
      let xLength: number = 0;
      let bottomXAxis = this.mChart.getBottomXAxis();
      if (bottomXAxis) {
        xLength = bottomXAxis.getAxisMaximum() - bottomXAxis.getAxisMinimum();
      }

      let positionStartX:number[] = [];
      let positionEndX:number[] = [];
      let yVals:number[] |null=barEntry.getYVals();
      if(yVals==null||yVals==undefined){
        yVals=[]
      }
      if(yVals.length>1){
        for(let yItem of yVals){
          let realStartPositionX=yItem> 0 ? 0 : Math.abs(yItem) / xLength  * this.width
          positionStartX.push(realStartPositionX)
          let realEndPositionX=yItem> 0 ?  Math.abs(yItem) / xLength  * this.width:0
          positionEndX.push(realEndPositionX)
        }
      }else{
        positionStartX[0]= barEntry.getX() > 0 ? 0 : Math.abs(barEntry.getX()) / xLength  * this.width
        positionEndX[0]= barEntry.getX() <0 ? 0 : Math.abs(barEntry.getX()) / xLength  * this.width
      }
      if (bottomXAxis) {
        let positionZero=this.mChart.finalViewPortHandler.contentLeft() + this.maxTextOffset + Math.abs(bottomXAxis.getAxisMinimum()) / xLength  * this.width;
        if (!this.mViewPortHandler) {
          return [];
        }
        for(let k=0;k<positionStartX.length;k++){
          buffer.buffer[j] = positionZero - positionStartX[k];
          buffer.buffer[j + 1] = this.singleHeight * number + this.mViewPortHandler.offsetTop() + this.marginBottom - this.mViewPortHandler.contentHeight() + this.mChart.finalViewPortHandler.contentHeight()
          buffer.buffer[j + 2] =  positionEndX[k] +positionZero;
          buffer.buffer[j + 3] = this.singleHeight * (number + 1) + this.mViewPortHandler.offsetTop() - this.marginBottom- this.mViewPortHandler.contentHeight() + this.mChart.finalViewPortHandler.contentHeight()
          if (!this.mViewPortHandler.isInBoundsLeft(buffer.buffer[j]))
            continue;

          if (!this.mViewPortHandler.isInBoundsRight(buffer.buffer[j + 2]))
            break;

          if (!isSingleColor) {
            // Set the color for the currently drawn value. If the index
            // is out of bounds, reuse colors.
            if(yVals.length>1){
              this.mRenderPaint.setColor(dataSet.getColor(k));
            }else{
              this.mRenderPaint.setColor(dataSet.getColor(pos));
            }
          }

          if (isCustomFill) {
            let rectPaint:RectPaint=new RectPaint();
            let style = this.mRenderPaint.getStyle();
            if (style) {
              rectPaint.setStyle(style)
            }
            rectPaint.setColor(this.mRenderPaint.getColor())
            let dataFills = dataSet.getFill(pos);
            if (dataFills) {
              let paint = dataFills
                .fillRect(
                  rectPaint,
                  buffer.buffer[j],
                  buffer.buffer[j + 1],
                  buffer.buffer[j + 2],
                  buffer.buffer[j + 3],
                  isInverted ? MyDirection.LEFT : MyDirection.RIGHT);
              if (paint) {
                paint.setStroke(this.mBarBorderPaint.getColor())
                paint.setStrokeWidth(this.mBarBorderPaint.getStrokeWidth())
                rectPaintArr.push(paint);
              }
            }
          } else {
            let rectPaint:RectPaint=new RectPaint();
            let style = this.mRenderPaint.getStyle();
            if (style) {
              rectPaint.setStyle(style)
            }

            rectPaint.setColor(this.mRenderPaint.getColor())
            rectPaint.setX(buffer.buffer[j])
            rectPaint.setY(buffer.buffer[j + 1])
            rectPaint.setWidth(Math.abs(buffer.buffer[j + 2]-buffer.buffer[j]))
            rectPaint.setHeight(Math.abs( buffer.buffer[j + 3]-buffer.buffer[j + 1]))
            rectPaint.setStroke(this.mBarBorderPaint.getColor())
            rectPaint.setStrokeWidth(this.mBarBorderPaint.getStrokeWidth())
            rectPaintArr.push(rectPaint)
          }

          //        if (drawBorder) {
          //          let rectPaint:RectPaint=new RectPaint(this.mBarBorderPaint as RectPaint);
          //          rectPaint.setX(buffer.buffer[j])
          //          rectPaint.setY(buffer.buffer[j + 1])
          //          rectPaint.setWidth(Math.abs( buffer.buffer[j + 2]-buffer.buffer[j]))
          //          rectPaint.setHeight(Math.abs( buffer.buffer[j + 3]-buffer.buffer[j + 1]))
          //          rectPaintArr.push(rectPaint)
          //        }
        }

        position++;
        number++;
      }
    }
    return rectPaintArr
  }

  protected  prepareBarHighlight(x:number, y1:number, y2:number, barWidthHalf:number, trans:Transformer):void {

    let left:number = x - barWidthHalf;
    let right:number = x + barWidthHalf;
    let top:number = y1;
    let bottom:number = y2;

    this.mBarRect.set(left, top, right, bottom);

    trans.rectToPixelPhase(this.mBarRect, this.mAnimator.getPhaseY());
  }

  public drawValues():Paint[] {
    if (!this.mChart) {
      return [];
    }
    let paintArr:Paint[]=new Array();

    // if values are drawn
    if (this.mChart.isDrawingValuesAllowed()) {
      let barData = this.mChart.getBarData();
      if (!barData) {
        return [];
      }
      let dataSets:JArrayList<IBarDataSet> = barData.getDataSets();

      let valueOffsetPlus:number = 4.5;
      let posOffset:number = 0;
      let negOffset:number = 0;
      let drawValueAboveBar:boolean = this.mChart.isDrawValueAboveBarEnabled();

      for (let i = 0; i < barData.getDataSetCount(); i++) {

        let dataSet:IBarDataSet | null = dataSets.get(i);
        if (!dataSet) {
          return [];
        }
        if (!this.shouldDrawValues(dataSet))
        continue;

        // apply the text-styling defined by the DataSet
        this.applyValueTextStyle(dataSet);

        // get the buffer
        if (!this.mBarBuffers) {
          break;
        }
        let buffer:BarBuffer = this.mBarBuffers[i];

        let phaseY:number = this.mAnimator.getPhaseY();

        let iconsOffset:MPPointF = MPPointF.getInstance(undefined,undefined,dataSet.getIconsOffset());
        if(iconsOffset.x != undefined){
          iconsOffset.x = Utils.convertDpToPixel(iconsOffset.x);
        }
        if(iconsOffset.y != undefined){
          iconsOffset.y = Utils.convertDpToPixel(iconsOffset.y);
        }

        // if only single values are drawn (sum)
        // if (!dataSet.isStacked()) {
        let position:number = 0;
        let number = 0;
        for(let j = 0; j < i; j++){

          let dataSet:IBarDataSet | null = this.mChart.getBarData()!.getDataSetByIndex(i - 1);
          if (dataSet) {
            let entries = dataSet.getEntries();
            if (entries) {
              number += entries.size();
            }
          }
        }
        for (let j = 0; j < buffer.buffer.length * this.mAnimator.getPhaseX(); j += 4) {
          if(position==dataSet.getEntryCount()){
            continue
          }
          let barEntry:BarEntry | null= dataSet.getEntries()!.get(position) as BarEntry;
          if (!barEntry) {
            break;
          }
          let xLength: number = 0;
          let bottomAxis = this.mChart.getBottomXAxis();
          if (bottomAxis) {
            xLength = bottomAxis.getAxisMaximum() - bottomAxis.getAxisMinimum();
          }

          let positionStartX:number[] = [];
          let positionEndX:number[] = [];
          let yVals:number[] | null=barEntry.getYVals();
          if(yVals==null||yVals==undefined){
            yVals=[]
          }
          if(yVals.length>1){
            for(let yItem of yVals){
              let realStartPositionX=yItem> 0 ? 0 : Math.abs(yItem) / xLength  * this.width
              positionStartX.push(realStartPositionX)
              let realEndPositionX=yItem> 0 ?  Math.abs(yItem) / xLength  * this.width:0
              positionEndX.push(realEndPositionX)
            }
          }else{
            positionStartX[0]= barEntry.getX() > 0 ? 0 : Math.abs(barEntry.getX()) / xLength  * this.width
            positionEndX[0]= barEntry.getX() <0 ? 0 : Math.abs(barEntry.getX()) / xLength  * this.width
          }
          if (bottomAxis) {
            let positionZero=this.mChart.finalViewPortHandler.contentLeft() + this.maxTextOffset + Math.abs(bottomAxis.getAxisMinimum()) / xLength  * this.width;
            if (!this.mViewPortHandler) {
              return []
            }
            for(let k=0;k<positionStartX.length;k++) {
              buffer.buffer[j] = positionZero - positionStartX[k];
              buffer.buffer[j + 1] = this.singleHeight * number + this.mViewPortHandler.offsetTop() + this.marginBottom - this.mViewPortHandler.contentHeight() + this.mChart.finalViewPortHandler.contentHeight()
              buffer.buffer[j + 2] =  positionEndX[k] +positionZero;
              buffer.buffer[j + 3] = this.singleHeight * (number + 1) + this.mViewPortHandler.offsetTop() - this.marginBottom- this.mViewPortHandler.contentHeight() + this.mChart.finalViewPortHandler.contentHeight()

              let x:number = (buffer.buffer[j] + buffer.buffer[j + 2]) / 2;
              let axisDependency = AxisDependency.LEFT;
              if (dataSet.getAxisDependency()) {
                axisDependency = dataSet.getAxisDependency();
              }
              let isInverted:boolean = this.mChart.isInverted(axisDependency);

              // calculate the correct offset depending on the draw position of
              // the value
              let valueTextHeight:number = Utils.calcTextHeight(this.mValuePaint, "8");
              posOffset = (drawValueAboveBar ? -valueOffsetPlus : valueTextHeight + valueOffsetPlus);
              negOffset = (drawValueAboveBar ? valueTextHeight + valueOffsetPlus : -valueOffsetPlus);

              if (isInverted) {
                posOffset = -posOffset - valueTextHeight;
                negOffset = -negOffset - valueTextHeight;
              }
              let entry = dataSet.getEntryForIndex(j / 4);
              if (!entry) {
                return [];
              }
              posOffset = 3;
              let val=yVals.length>1?yVals[k]:barEntry.getX()
              if(val<0){
                let valueFormatter = dataSet.getValueFormatter();
                if (valueFormatter && this.mViewPortHandler) {
                  let resultValue:string=valueFormatter.getFormattedValue(val, entry, i, this.mViewPortHandler)
                  let textWidth=Utils.calcTextWidth(this.mValuePaint,resultValue)+posOffset*2
                  posOffset-=textWidth
                }
              }
              negOffset = ((buffer.buffer[j + 3] - buffer.buffer[j + 1]) / 2) - (Utils.calcTextHeight(this.mValuePaint,"8") / 2);
              if (dataSet.isDrawValuesEnabled()) {
                let textX=val<0?buffer.buffer[j] + posOffset: buffer.buffer[j + 2] + posOffset;
                let valueFormatter = dataSet.getValueFormatter();
                if (valueFormatter) {
                  paintArr = paintArr.concat(this.drawValue(valueFormatter, val, entry, i,textX,
                    buffer.buffer[j + 1] + negOffset,
                    dataSet.getValueTextColor(j / 4)));
                }
              }

              if (entry.getIcon() != null && dataSet.isDrawIconsEnabled()) {

                let icon:ImagePaint | null= entry.getIcon();
                if (icon) {
                  let px:number = x;
                  let py:number = val >= 0 ?
                    (buffer.buffer[j + 1] + posOffset) :
                    (buffer.buffer[j + 3] + negOffset);

                  px += iconsOffset.x;
                  py += iconsOffset.y;

                  paintArr = paintArr.concat(Utils.drawImage(
                    icon.getIcon(),
                    px,
                    py,
                    Number(icon.getWidth()),
                    Number(icon.getHeight())));
                }
              }
            }
            position++;
            number++;
          }
        }
        MPPointF.recycleInstance(iconsOffset);
      }
    }
    return paintArr;
  }

  public drawHighlighted(indices:Highlight[]):Paint[] {
    let rectPaintArr:RectPaint[]=new Array();
    let barData:BarData  | null= this.mChart!.getBarData();
    if (!barData || !this.mChart) {
      return [];
    }
    for (let high of indices) {

      let dataSet:IBarDataSet | null= barData.getDataSetByIndex(high.getDataSetIndex());

      if (dataSet == null || !dataSet.isHighlightEnabled())
      continue;

      let e = dataSet.getEntryForXValue(high.getX(), high.getY());
      if (!e) {
        continue;
      }
      if (!this.isInBoundsX(e, dataSet))
      continue;
      let axisDependency= AxisDependency.LEFT;
      if (dataSet.getAxisDependency()) {
        axisDependency = dataSet.getAxisDependency();
      }
      let trans:Transformer | null= this.mChart.getTransformer(axisDependency);

      this.mHighlightPaint.setColor(dataSet.getHighLightColor());
      this.mHighlightPaint.setAlpha(dataSet.getHighLightAlpha());

      let isStack:boolean = (high.getStackIndex() >= 0  && (e as BarEntry).isStacked()) ? true : false;

      let y1:number = 0;
      let y2:number =0;

      if (isStack) {

        if(this.mChart.isHighlightFullBarEnabled()) {

          y1 = (e as BarEntry).getPositiveSum();
          y2 = -(e as BarEntry).getNegativeSum();

        } else {
          let rangeArray  = (e as BarEntry).getRanges();
          if (rangeArray) {
            let range:Range = rangeArray[high.getStackIndex()];

            y1 = range.myfrom;
            y2 = range.to;
          }
        }

      } else {
        y1 = e.getY();
        y2 = 0.;
      }

      if(trans){
        this.prepareBarHighlight(e.getX(), y1, y2, barData.getBarWidth() / 2, trans);
      }

      this.setHighlightDrawPos(high, this.mBarRect);
      let rectPaint:RectPaint=new RectPaint();
      let style = this.mHighlightPaint.getStyle();
      if (style) {
        rectPaint.setStyle(style);
      }
      rectPaint.setStrokeWidth(this.mHighlightPaint.getStrokeWidth());
      rectPaint.setColor(this.mHighlightPaint.getColor());
      rectPaint.setAlpha(this.mHighlightPaint.getAlpha());
      rectPaint.setX(this.mBarRect.left)
      rectPaint.setY(this.mBarRect.top)
      rectPaint.setWidth(this.mBarRect.right-this.mBarRect.left)
      rectPaint.setHeight(this.mBarRect.bottom-this.mBarRect.top)
      rectPaintArr.push(rectPaint);
    }
    return rectPaintArr
  }

  /**
     * Sets the drawing position of the highlight object based on the riven bar-rect.
     * @param high
     */
  protected  setHighlightDrawPos( high:Highlight,  bar:MyRect):void {
    high.setDraw(bar.centerX(), bar.top);
  }

  public drawExtras():Paint[] | null{
    return null;
  }

  public getMarginBottom(): number{
    return this.marginBottom;
  }

  public drawClicked(paint:Paint,paint2:TextPaint,count: number,position: number): Paint[]{
    let rectPaint = new RectPaint();
    rectPaint.set(paint);
    rectPaint.setGradientFillColor([[0x80000000,1],[0x80000000,1]]);
    rectPaint.setClickPosition(position)
    return [rectPaint];
  }

}
