/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Paint, { TextPaint, CirclePaint,ImagePaint}  from '../../data/Paint';
import BubbleChartMode from '../../data/BubbleChartMode';

@Component
export default struct BubbleView {
  positionX: number= 0;
  @Link
  bubbleChartMode:BubbleChartMode

  public aboutToAppear() {
    this.bubbleChartMode.drawChart()
  }
  build() {
    Stack({ alignContent: Alignment.TopStart }) {
        if (this.bubbleChartMode.paints != null && this.bubbleChartMode.paints != undefined
        && this.bubbleChartMode.paints.length > 0) {

          ForEach(this.bubbleChartMode.paints, (item: Paint) => {

            if (item instanceof CirclePaint) {
              Circle()
                .width(item.width)
                .height(item.height)
                .fill(item.color)
                .position({ x: item.x, y: item.y })
                .zIndex(1)
            } else if (item instanceof TextPaint) {
              Text(item.text)
                .position({ x: item.x, y: item.y })
                .fontWeight(item.typeface)
                .fontSize(item.textSize)
                .textAlign(item.textAlign)
                .fontColor(item.color)
                .zIndex(2)
            } else if (item instanceof ImagePaint) {
              Image(item.getIcon())
                .position({ x: item.x, y: item.y })
                .width(item.width)
                .height(item.height).zIndex(3)
            }
          }, (item: Paint) => JSON.stringify(item))

        }
      if (this.bubbleChartMode.indexHighLightPaint != null && this.bubbleChartMode.indexHighLightPaint != undefined
      && this.bubbleChartMode.paints.length > 0) {
        ForEach(this.bubbleChartMode.highLight, (item: Paint) => {
          if (item instanceof CirclePaint) {
            Shape() {
              Circle()
                .width(item.width)
                .height(item.height)
                .fill(item.stroke)
                .strokeWidth(10)
                .strokeWidth(item.getStrokeWidth())
                .position({ x: item.x, y: item.y })
            }
          }
        }, (item: Paint) => JSON.stringify(item))
      }
    }.onClick((event: ClickEvent | undefined)=>{
      if(this.bubbleChartMode.isNeedScale && event){
        this.bubbleChartMode.onClick(event)
      }

    }).onTouch((event: TouchEvent | undefined) => {
      if(this.bubbleChartMode.isNeedScale && event){
        this.bubbleChartMode.onTouch(event)
      }
    }).width(this.bubbleChartMode.mWidth).height(this.bubbleChartMode.mHeight)
    .position({ x: 0, y:0})
    .clip(new Path().commands(this.bubbleChartMode.clipPath)).zIndex(1)
  }
}

