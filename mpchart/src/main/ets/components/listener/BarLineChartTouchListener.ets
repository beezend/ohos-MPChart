/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Highlight from '../highlight/Highlight';
import ViewPortHandler from '../utils/ViewPortHandler';
import MPPointF from '../utils/MPPointF';
import Matrix from '../utils/Matrix';
import ChartTouchListener, {ChartGesture} from './ChartTouchListener'
import BarLineChartBase from '../charts/BarLineChartBase'
import IDataSet from '../interfaces/datasets/IDataSet'
import Utils from '../utils/Utils'
import OnChartGestureListener from './OnChartGestureListener'
import HorizontalBarChart from '../charts/HorizontalBarChart'

/**
 * TouchListener for Bar-, Line-, Scatter- and CandleStickChart with handles all
 * touch interaction. Longpress == Zoom out. Double-Tap == Zoom in.
 *
 * @author Philipp Jahoda
 */
export default class BarLineChartTouchListener extends ChartTouchListener<BarLineChartBase> {

  /**
    * the original touch-matrix from the chart
    */
  private mMatrix: Matrix = new Matrix();

  /**
    * matrix for saving the original matrix state
    */
  private mSavedMatrix: Matrix = new Matrix();

  /**
    * point where the touch action started
    */
  private mTouchStartPoint: MPPointF = MPPointF.getInstance(0, 0);

  /**
    * center between two pointers (fingers on the display)
    */
  private mTouchPointCenter: MPPointF = MPPointF.getInstance(0, 0);
  private mSavedXDist: number = 1;
  private mSavedYDist: number = 1;
  private mSavedDist: number = 1;
  private mClosestDataSetToTouch: IDataSet<any>;

  /**
       * used for tracking velocity of dragging
       */
  //  private mVelocityTracker: VelocityTracker;
  private mDecelerationLastTime: number = 0;
  private mDecelerationCurrentPoint: MPPointF = MPPointF.getInstance(0, 0);
  private mDecelerationVelocity: MPPointF = MPPointF.getInstance(0, 0);

  /**
    * the distance of movement that will be counted as a drag
    */
  private mDragTriggerDist: number;

  /**
    * the minimum distance between the pointers that will trigger a zoom gesture
    */
  private mMinScalePointerDistance: number;

  /**
    * Constructor with initialization parameters.
    *
    * @param chart               instance of the chart
    * @param touchMatrix         the touch-matrix of the chart
    * @param dragTriggerDistance the minimum movement distance that will be interpreted as a "drag" gesture in dp (3dp equals
    *                            to about 9 pixels on a 5.5" FHD screen)
    */
  constructor(chart: BarLineChartBase, touchMatrix: Matrix, dragTriggerDistance: number) {
    super(chart);
    this.mMatrix = touchMatrix;

    this.mDragTriggerDist = Utils.convertDpToPixel(dragTriggerDistance);

    this.mMinScalePointerDistance = Utils.convertDpToPixel(3.5);
  }

  public onTouch(event: TouchEvent) {
    if (!this.mChart.isDragEnabled() && (!this.mChart.isScaleXEnabled() && !this.mChart.isScaleYEnabled()))
    return true;

    // Handle touch events here...
    switch (event.type) {

      case TouchType.Down:
        this.startAction(event);
        this.stopDeceleration();
        this.saveTouchStart(event);

        if (event.touches.length >= 2) {
          this.mChart.disableScroll();
          this.saveTouchStart(event);
          // get the distance between the pointers on the x-axis
          this.mSavedXDist = BarLineChartTouchListener.getXDist(event);
          // get the distance between the pointers on the y-axis
          this.mSavedYDist = BarLineChartTouchListener.getYDist(event);
          // get the total distance between the pointers
          this.mSavedDist = BarLineChartTouchListener.spacing(event);
          if (this.mSavedDist > 10) {

            if (this.mChart.isPinchZoomEnabled()) {
              this.mTouchMode = ChartTouchListener.PINCH_ZOOM;
            } else {
              if (this.mChart.isScaleXEnabled() != this.mChart.isScaleYEnabled()) {
                this.mTouchMode = this.mChart.isScaleXEnabled() ? ChartTouchListener.X_ZOOM : ChartTouchListener.Y_ZOOM;
              } else {
                this.mTouchMode = this.mSavedXDist > this.mSavedYDist ? ChartTouchListener.X_ZOOM : ChartTouchListener.Y_ZOOM;
              }
            }
          }
          // determine the touch-pointer center
          BarLineChartTouchListener.midPoint(this.mTouchPointCenter, event);
        }
        break;


      case TouchType.Move:

        if (this.mTouchMode == ChartTouchListener.DRAG) {

          this.mChart.disableScroll();
          let x: number = this.mChart.isDragXEnabled() ? event.touches[0].x - this.mTouchStartPoint.x : 0;
          let y: number = this.mChart.isDragYEnabled() ? event.touches[0].y - this.mTouchStartPoint.y : 0;
          this.performDrag(event, x, y);
        } else if (this.mTouchMode == ChartTouchListener.X_ZOOM || this.mTouchMode == ChartTouchListener.Y_ZOOM || this.mTouchMode == ChartTouchListener.PINCH_ZOOM) {

          this.mChart.disableScroll();
          if (this.mChart.isScaleXEnabled() || this.mChart.isScaleYEnabled())
          this.performZoom(event);
        } else if (this.mTouchMode == ChartTouchListener.NONE
        && Math.abs(ChartTouchListener.distance(event.touches[0].x, this.mTouchStartPoint.x, event.touches[0].y,
          this.mTouchStartPoint.y)) > this.mDragTriggerDist) {

          if (this.mChart.isDragEnabled()) {

            let shouldPan: boolean = !this.mChart.isFullyZoomedOut() ||
            !this.mChart.hasNoDragOffset();
            if (shouldPan) {

              let distanceX: number = Math.abs(event.touches[0].x - this.mTouchStartPoint.x);
              let distanceY: number = Math.abs(event.touches[0].y - this.mTouchStartPoint.y);
              // Disable dragging in a direction that's disallowed
              if ((this.mChart.isDragXEnabled() || distanceY >= distanceX) &&
              (this.mChart.isDragYEnabled() || distanceY <= distanceX)) {

                this.mLastGesture = ChartGesture.DRAG;
                this.mTouchMode = ChartTouchListener.DRAG;
              }
            } else {

              if (this.mChart.isHighlightPerDragEnabled()) {
                this.mLastGesture = ChartGesture.DRAG;

                if (this.mChart.isHighlightPerDragEnabled())
                this.performHighlightDrag(event);
              }
            }
          }

        }
        break;

      case TouchType.Up:

        const pointerId: number = event.touches[0].id;

        if (Math.abs(event.touches[0].x) > Utils.getMinimumFlingVelocity() ||
        Math.abs(event.touches[0].y) > Utils.getMinimumFlingVelocity()) {

          if (this.mTouchMode == ChartTouchListener.DRAG && this.mChart.isDragDecelerationEnabled()) {

            this.stopDeceleration();

            //            this.mDecelerationLastTime = AnimationUtils.currentAnimationTimeMillis();

            this.mDecelerationCurrentPoint.x = event.touches[0].x;
            this.mDecelerationCurrentPoint.y = event.touches[0].y;

            //            this.mDecelerationVelocity.x = velocityX;
            //            this.mDecelerationVelocity.y = velocityY;

            //            Utils.postInvalidateOnAnimation(this.mChart); // This causes computeScroll to fire, recommended for this by
            // Google
          }
        }

        if (this.mTouchMode == ChartTouchListener.X_ZOOM ||
        this.mTouchMode == ChartTouchListener.Y_ZOOM ||
        this.mTouchMode == ChartTouchListener.PINCH_ZOOM ||
        this.mTouchMode == ChartTouchListener.POST_ZOOM) {

          // Range might have changed, which means that Y-axis labels
          // could have changed in size, affecting Y-axis size.
          // So we need to recalculate offsets.
          this.mChart.calculateOffsets();
          this.mChart.postInvalidate();
        }

        this.mTouchMode = ChartTouchListener.NONE;
        this.mChart.enableScroll();

        this.endAction(event);

      //        Utils.velocityTrackerPointerUpCleanUpIfNecessary(event, mVelocityTracker);

        this.mTouchMode = ChartTouchListener.POST_ZOOM;
        break;

      case TouchType.Cancel:

        this.mTouchMode = ChartTouchListener.NONE;
        this.endAction(event);
        break;
    }

    // perform the transformation, update the chart
    this.mMatrix = this.mChart.getViewPortHandler().refresh(this.mMatrix, this.mChart, true);

    return true; // indicate event was handled
  }

  /**
       * ################ ################ ################ ################
       */
  /** BELOW CODE PERFORMS THE ACTUAL TOUCH ACTIONS */

  /**
    * Saves the current Matrix state and the touch-start point.
    *
    * @param event
    */
  private saveTouchStart(event: TouchEvent) {

    this.mSavedMatrix.set(this.mMatrix);
    this.mTouchStartPoint.x = event.touches[0].x;
    this.mTouchStartPoint.y = event.touches[0].y;

    this.mClosestDataSetToTouch = this.mChart.getDataSetByTouchPoint(event.touches[0].x, event.touches[0].y);
  }

  /**
    * Performs all necessary operations needed for dragging.
    *
    * @param event
    */
  private performDrag(event: TouchEvent, distanceX: number, distanceY: number) {

    this.mLastGesture = ChartGesture.DRAG;

    this.mMatrix.set(this.mSavedMatrix);

    let l: OnChartGestureListener = this.mChart.getOnChartGestureListener();

    // check if axis is inverted
    if (this.inverted()) {

      // if there is an inverted horizontalbarchart
      if (this.mChart instanceof HorizontalBarChart) {
        distanceX = -distanceX;
      } else {
        distanceY = -distanceY;
      }
    }

    this.mMatrix.postTranslate(distanceX, distanceY);

    if (l != null)
    l.onChartTranslate(event, distanceX, distanceY);
  }

  /**
    * Performs the all operations necessary for pinch and axis zoom.
    *
    * @param event
    */
  private performZoom(event: TouchEvent) {

    if (event.touches.length >= 2) { // two finger zoom

      let l: OnChartGestureListener = this.mChart.getOnChartGestureListener();

      // get the distance between the pointers of the touch event
      let totalDist: number = BarLineChartTouchListener.spacing(event);

      if (totalDist > this.mMinScalePointerDistance) {

        // get the translation
        let t: MPPointF = this.getTrans(this.mTouchPointCenter.x, this.mTouchPointCenter.y);
        let h: ViewPortHandler = this.mChart.getViewPortHandler();

        // take actions depending on the activated touch mode
        if (this.mTouchMode == ChartTouchListener.PINCH_ZOOM) {

          this.mLastGesture = ChartGesture.PINCH_ZOOM;

          let scale: number = this.totalDist / this.mSavedDist; // total scale

          let isZoomingOut: boolean = (scale < 1);

          let canZoomMoreX: boolean = isZoomingOut ?
          h.canZoomOutMoreX() :
          h.canZoomInMoreX();

          let canZoomMoreY: boolean = isZoomingOut ?
          h.canZoomOutMoreY() :
          h.canZoomInMoreY();

          let scaleX: number = (this.mChart.isScaleXEnabled()) ? scale : 1;
          let scaleY: number = (this.mChart.isScaleYEnabled()) ? scale : 1;

          if (canZoomMoreY || canZoomMoreX) {

            this.mMatrix.set(this.mSavedMatrix);
            this.mMatrix.postScale(scaleX, scaleY, t.x, t.y);

            if (l != null)
            l.onChartScale(event, scaleX, scaleY);
          }

        } else if (this.mTouchMode == ChartTouchListener.X_ZOOM && this.mChart.isScaleXEnabled()) {

          this.mLastGesture = ChartGesture.X_ZOOM;

          let xDist: number = BarLineChartTouchListener.getXDist(event);
          let scaleX: number = this.xDist / this.mSavedXDist; // x-axis scale

          let isZoomingOut: boolean = (scaleX < 1);
          let canZoomMoreX: boolean = isZoomingOut ?
          h.canZoomOutMoreX() :
          h.canZoomInMoreX();

          if (canZoomMoreX) {

            this.mMatrix.set(this.mSavedMatrix);
            this.mMatrix.postScale(scaleX, 1, t.x, t.y);

            if (l != null)
            l.onChartScale(event, scaleX, 1);
          }

        } else if (this.mTouchMode == ChartTouchListener.Y_ZOOM && this.mChart.isScaleYEnabled()) {

          this.mLastGesture = ChartGesture.Y_ZOOM;

          let yDist: number = BarLineChartTouchListener.getYDist(event);
          let scaleY: number = yDist / this.mSavedYDist; // y-axis scale

          let isZoomingOut: boolean = (scaleY < 1);
          let canZoomMoreY: boolean = isZoomingOut ?
          h.canZoomOutMoreY() :
          h.canZoomInMoreY();

          if (canZoomMoreY) {

            this.mMatrix.set(this.mSavedMatrix);
            this.mMatrix.postScale(1, scaleY, t.x, t.y);

            if (l != null)
            l.onChartScale(event, 1, scaleY);
          }
        }

        MPPointF.recycleInstance(t);
      }
    }
  }

  /**
    * Highlights upon dragging, generates callbacks for the selection-listener.
    *
    * @param e
    */
  private performHighlightDrag(e: TouchEvent) {

    let h: Highlight = this.mChart.getHighlightByTouchPoint(e.touches[0].x, e.touches[0].y);

    if (h != null && !h.equalTo(this.mLastHighlighted)) {
      this.mLastHighlighted = h;
      this.mChart.highlightValue(h, true);
    }
  }

  /**
    * ################ ################ ################ ################
    */
  /** DOING THE MATH BELOW ;-) */


  /**
    * Determines the center point between two pointer touch points.
    *
    * @param point
    * @param event
    */
  private static midPoint(point: MPPointF, event: TouchEvent) {
    let x: number = event.touches[0].x + event.touches[1].x;
    let y: number = event.touches[0].y + event.touches[1].y;
    point.x = (x / 2);
    point.y = (y / 2);
  }

  /**
    * returns the distance between two pointer touch points
    *
    * @param event
    * @return
    */
  private static spacing(event: TouchEvent): number {
    let x: number = event.touches[0].x - event.touches[1].x;
    let y: number = event.touches[0].y - event.touches[1].y;
    return Math.sqrt(x * x + y * y);
  }

  /**
    * calculates the distance on the x-axis between two pointers (fingers on
    * the display)
    *
    * @param e
    * @return
    */
  private static getXDist(e: TouchEvent): number {
    let x: number = Math.abs(e.touches[0].x - e.touches[1].x);
    return x;
  }

  /**
    * calculates the distance on the y-axis between two pointers (fingers on
    * the display)
    *
    * @param e
    * @return
    */
  private static getYDist(e: TouchEvent): number {
    let y: number = Math.abs(e.touches[0].y - e.touches[1].y);
    return y;
  }

  /**
    * Returns a recyclable MPPointF instance.
    * returns the correct translation depending on the provided x and y touch
    * points
    *
    * @param x
    * @param y
    * @return
    */
  public getTrans(x: number, y: number): MPPointF {

    let vph: ViewPortHandler = this.mChart.getViewPortHandler();

    let xTrans: number = x - vph.offsetLeft();
    let yTrans: number = 0;

    // check if axis is inverted
    if (this.inverted()) {
      yTrans = -(y - vph.offsetTop());
    } else {
      yTrans = -(this.mChart.getMeasuredHeight() - y - vph.offsetBottom());
    }

    return MPPointF.getInstance(xTrans, yTrans);
  }

  /**
    * Returns true if the current touch situation should be interpreted as inverted, false if not.
    *
    * @return
    */
  private inverted(): boolean {
    return (this.mClosestDataSetToTouch == null && this.mChart.isAnyAxisInverted()) || (this.mClosestDataSetToTouch != null
    && this.mChart.isInverted(this.mClosestDataSetToTouch.getAxisDependency()));
  }

  /**
    * ################ ################ ################ ################
    */
  /** GETTERS AND GESTURE RECOGNITION BELOW */

  /**
    * returns the matrix object the listener holds
    *
    * @return
    */
  public getMatrix(): Matrix {
    return this.mMatrix;
  }

  /**
    * Sets the minimum distance that will be interpreted as a "drag" by the chart in dp.
    * Default: 3dp
    *
    * @param dragTriggerDistance
    */
  public setDragTriggerDist(dragTriggerDistance: number) {
    this.mDragTriggerDist = Utils.convertDpToPixel(dragTriggerDistance);
  }

  public onDoubleTap(e: TouchEvent): boolean {

    this.mLastGesture = ChartGesture.DOUBLE_TAP;

    let l: OnChartGestureListener = this.mChart.getOnChartGestureListener();

    if (l != null) {
      l.onChartDoubleTapped(e);
    }

    // check if double-tap zooming is enabled
    if (this.mChart.isDoubleTapToZoomEnabled() && this.mChart.getData().getEntryCount() > 0) {

      let trans: MPPointF = this.getTrans(e.getX(), e.getY());

      let scaleX: number = this.mChart.isScaleXEnabled() ? 1.4 : 1;
      let scaleY: number = this.mChart.isScaleYEnabled() ? 1.4 : 1;

      this.mChart.zoom(scaleX, scaleY, trans.x, trans.y);

      if (this.mChart.isLogEnabled())
      console.info("BarlineChartTouch:" + "Double-Tap, Zooming In, x: " + trans.x + ", y: "
      + trans.y)

      if (l != null) {
        l.onChartScale(e, scaleX, scaleY);
      }

      MPPointF.recycleInstance(trans);
    }

    return false;
  }

  public onLongPress(e: TouchEvent) {

    this.mLastGesture = ChartGesture.LONG_PRESS;

    let l: OnChartGestureListener = this.mChart.getOnChartGestureListener();

    if (l != null) {

      l.onChartLongPressed(e);
    }
  }

  public onSingleTapUp(e: TouchEvent): boolean {

    this.mLastGesture = ChartGesture.SINGLE_TAP;

    let l: OnChartGestureListener = this.mChart.getOnChartGestureListener();

    if (l != null) {
      l.onChartSingleTapped(e);
    }

    if (!this.mChart.isHighlightPerTapEnabled()) {
      return false;
    }

    let h: Highlight = this.mChart.getHighlightByTouchPoint(e.touches[0].x, e.touches[0].y);
    this.performHighlight(h, e);

    return false;
  }

  public onFling(e1: TouchEvent, e2: TouchEvent, velocityX: number, velocityY: number): boolean {

    this.mLastGesture = ChartGesture.FLING;

    let l: OnChartGestureListener = this.mChart.getOnChartGestureListener();

    if (l != null) {
      l.onChartFling(e1, e2, velocityX, velocityY);
    }

    return false;
  }

  public stopDeceleration() {
    this.mDecelerationVelocity.x = 0;
    this.mDecelerationVelocity.y = 0;
  }

  public computeScroll() {

    if (this.mDecelerationVelocity.x == 0 && this.mDecelerationVelocity.y == 0)
    return; // There's no deceleration in progress

    const currentTime:number = AnimationUtils.currentAnimationTimeMillis();

    this.mDecelerationVelocity.x *= this.mChart.getDragDecelerationFrictionCoef();
    this.mDecelerationVelocity.y *= this.mChart.getDragDecelerationFrictionCoef();

    const timeInterval:number = (this.currentTime - this.mDecelerationLastTime) / 1000;

    let distanceX:number = this.mDecelerationVelocity.x * timeInterval;
    let distanceY:number = this.mDecelerationVelocity.y * timeInterval;

    this.mDecelerationCurrentPoint.x += distanceX;
    this.mDecelerationCurrentPoint.y += distanceY;

    let event:TouchEvent = TouchEvent.obtain(currentTime, currentTime, MotionEvent.ACTION_MOVE, mDecelerationCurrentPoint.x,
      mDecelerationCurrentPoint.y, 0);

    let dragDistanceX:number = this.mChart.isDragXEnabled() ? this.mDecelerationCurrentPoint.x - this.mTouchStartPoint.x : 0;
    let dragDistanceY:number = this.mChart.isDragYEnabled() ? this.mDecelerationCurrentPoint.y - this.mTouchStartPoint.y : 0;

    this.performDrag(event, dragDistanceX, dragDistanceY);

    event.recycle();
    this.mMatrix = this.mChart.getViewPortHandler().refresh(this.mMatrix, this.mChart, false);

    this.mDecelerationLastTime = currentTime;

    if (Math.abs(this.mDecelerationVelocity.x) >= 0.01 || Math.abs(this.mDecelerationVelocity.y) >= 0.01)
    Utils.postInvalidateOnAnimation(this.mChart); // This causes computeScroll to fire, recommended for this by Google
    else {
      // Range might have changed, which means that Y-axis labels
      // could have changed in size, affecting Y-axis size.
      // So we need to recalculate offsets.
      this.mChart.calculateOffsets();
      this.mChart.postInvalidate();

      this.stopDeceleration();
    }
  }
}
