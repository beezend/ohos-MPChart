/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import ChartAnimator from '../animation/ChartAnimator';
import ViewPortHandler from '../utils/ViewPortHandler';
import { XAxis } from '../components/XAxis';
import YAxis from '../components/YAxis'
import ScatterData from './ScatterData';
import LimitLine from '../components/LimitLine';
import ScatterChartRenderer from '../renderer/ScatterChartRenderer';
import Paint, { IndexPositionPaint, TextPaint } from './Paint';
import EntryOhos from './EntryOhos';
import Highlight from '../highlight/Highlight';
import ScaleMode from './ScaleMode';
import Utils from '../utils/Utils';
import MyRect from './Rect';

@Observed
export default class ScaterChartMode extends ScaleMode {
  topAxis: XAxis = new XAxis(); //顶部X轴
  bottomAxis: XAxis = new XAxis(); //底部X轴
  mWidth: number = 350; //表的宽度
  mHeight: number = 300; //表的高度
  minOffset: number = 15; //X轴线偏移量
  leftAxis: YAxis = new YAxis();
  rightAxis: YAxis = new YAxis();
  XLimtLine: LimitLine = new LimitLine(35, "Index 10");
  data: ScatterData = new ScatterData();
  paints: Paint[] = []
  highLight: Paint[] = []
  indexHighLightPaint: IndexPositionPaint | null = null;
  xScale: number = 1;
  yScale: number = 1;
  handler: ViewPortHandler = new ViewPortHandler();
  mAnimator: ChartAnimator = new ChartAnimator();
  scatterRender: ScatterChartRenderer | null = null;
  public xExtraOffset = 0
  public yExtraOffset = 0
  maxVisiableIndex = Number.MAX_VALUE

  public setYExtraOffset(yExtraOffset: number) {
    this.yExtraOffset = yExtraOffset
    return this
  }

  public calcScale() {
    let rect = this.data.mDisplayRect;
    if (this.topAxis && this.leftAxis) {
      let minX = this.topAxis.getAxisMinimum()
      let miny = this.leftAxis.getAxisMinimum()
      this.xScale = (rect.right - rect.left) / (this.topAxis.getAxisMaximum() - minX);
      this.yScale = (rect.bottom - rect.top) / (this.leftAxis.getAxisMaximum() - miny);
    }
  }

  public init() {
    this.calcScale();
    this.handler = new ViewPortHandler();
    this.handler.restrainViewPort(this.minOffset, this.minOffset, this.minOffset, this.minOffset)
    this.handler.setChartDimens(this.mWidth, this.mHeight);
    this.mAnimator = new ChartAnimator();
    this.scatterRender = new ScatterChartRenderer(this);
  }

  public calcClipPath(num: number): string {
    let rect = this.data.mDisplayRect;
    let path = ''
    if (num == 1) {
      path = 'M' + Utils.convertDpToPixel(rect.left) + ' ' + 0
        + 'L' + Utils.convertDpToPixel(rect.right) + ' ' + 0
        + 'L' + Utils.convertDpToPixel(rect.right) + ' ' + Utils.convertDpToPixel(rect.bottom - this.minOffset)
        + 'L' + Utils.convertDpToPixel(rect.left) + ' ' + Utils.convertDpToPixel(rect.bottom - this.minOffset)
        + ' Z'
    } else if (num == 2) {
      path = 'M' + Utils.convertDpToPixel(rect.left - 2) + ' ' + Utils.convertDpToPixel(0)
        + 'L' + Utils.convertDpToPixel(rect.right + 8) + ' ' + Utils.convertDpToPixel(0)
        + 'L' + Utils.convertDpToPixel(rect.right + 8) + ' ' + Utils.convertDpToPixel(this.mHeight)
        + 'L' + Utils.convertDpToPixel(rect.left - 2) + ' ' + Utils.convertDpToPixel(this.mHeight)
        + ' Z'
    } else {
      // y
      path = 'M' + Utils.convertDpToPixel(rect.left - 30) + ' ' + Utils.convertDpToPixel(rect.top)
        + 'L' + Utils.convertDpToPixel(rect.right + 8) + ' ' + Utils.convertDpToPixel(rect.top)
        + 'L' + Utils.convertDpToPixel(rect.right + 8) + ' ' + Utils.convertDpToPixel(this.mHeight)
        + 'L' + Utils.convertDpToPixel(rect.left - 30) + ' ' + Utils.convertDpToPixel(this.mHeight)
        + ' Z'
    }
    return path
  }

  public invalidate() {
    let textPaint: TextPaint = new TextPaint();
    if (this.leftAxis) {
      textPaint.setTextSize(this.leftAxis.getTextSize());
      let leftTextWidth = Utils.calcTextWidth(textPaint, this.leftAxis.getAxisMaximum() + "");
      let left = this.leftAxis.getSpaceTop() + leftTextWidth;
      let top = this.minOffset;
      let right = this.mWidth - this.minOffset - leftTextWidth;
      let bottom = this.mHeight - this.minOffset;
      this.data.mDisplayRect = new MyRect(left, top, right, bottom);
      this.drawChart();
      this.drawHighLight();
    }
  }


  public drawChart() {
    let paintsTemp: Paint[] = [];
    if (this.scatterRender) {
      paintsTemp = paintsTemp.concat(this.scatterRender.drawData());
      paintsTemp = paintsTemp.concat(this.scatterRender.drawValues());
    }

    this.paints = []
    this.paints = this.paints.concat(paintsTemp)
  }

  public drawHighLight() {
    if (this.indexHighLightPaint == null || !this.data.isHighlightEnabled()) {
      this.indexHighLightPaint = null;
      return
    }
    let hightL: Highlight = new Highlight(this.indexHighLightPaint.x, this.indexHighLightPaint.y,
      this.indexHighLightPaint.dataSetIndex, this.indexHighLightPaint.dataIndex);
    if (this.scatterRender) {
      this.highLight = this.scatterRender.drawHighlighted([hightL]);
    }
  }

  public animate(animateAxis: string, animateTime: number, spaceTime: number) {
    this.paints = []
    this.highLight = []
    let that = this
    let maxCountEntrySet = this.data.getMaxEntryCountSet();
    if (maxCountEntrySet) {
      let maxCount = maxCountEntrySet.getEntryCount();
      let everySpaceX = maxCount / (animateTime / spaceTime)
      let everySpaceY = 10 / (animateTime / spaceTime)

      let currentPhaseX = 0
      let currentPhaseY = 0
      let intervalID = setInterval( () => {
        if (currentPhaseX != 0) {
          if (animateAxis == 'X') {
            that.maxVisiableIndex = currentPhaseX;
          } else if (animateAxis == 'Y') {
            if (that.mAnimator) {
              that.mAnimator.setPhaseY(currentPhaseY / 10)
            }
          } else if (animateAxis == 'XY') {
            that.maxVisiableIndex = currentPhaseX;
            if (that.mAnimator) {
              that.mAnimator.setPhaseY(currentPhaseY / 10)
            }
          }
          that.drawChart();
          that.drawHighLight();
        }
        currentPhaseX += everySpaceX
        currentPhaseY += everySpaceY
        animateTime -= spaceTime
        if (animateTime < 0) {
          clearInterval(intervalID);
        }
      }, spaceTime);
    }

  }

  public onDoubleClick(event?: ClickEvent) {
    this.scaleX = Number(this.scaleX.toFixed(1))
    this.scaleY = Number(this.scaleY.toFixed(1))
    console.log('this.scaleX:' + this.scaleX)
    this.currentXSpace = this.centerX * this.scaleX - this.centerX
    this.currentYSpace = this.centerY * this.scaleY - this.centerY
    let moveYSource = this.leftAxisModel.lastHeight * this.scaleY - this.leftAxisModel.lastHeight
    this.leftAxisModel.translate(moveYSource + this.moveY - this.currentYSpace);
    this.setXPosition(this.moveX - this.currentXSpace)
    this.drawChart()
    this.drawHighLight()
  }

  public onMove(event: TouchEvent) {
    let finalMoveX = this.currentXSpace - this.moveX
    let finalMoveY = this.currentYSpace - this.moveY
    if (this.moveX > 0 && finalMoveX <= 0) {
      this.moveX = this.currentXSpace
    }
    if (this.moveY > 0 && finalMoveY <= 0) {
      this.moveY = this.currentYSpace
    }
    if (this.moveX - this.currentXSpace <= this.mWidth - this.xAixsMode.mWidth) {
      this.moveX = this.mWidth - this.xAixsMode.mWidth + this.currentXSpace
    }
    let scaleYHeight = this.mHeight * this.scaleY
    if (this.moveY - this.currentYSpace <= this.mHeight - scaleYHeight) {
      this.moveY = this.mHeight - scaleYHeight + this.currentYSpace
    }

    let moveYSource = this.leftAxisModel.lastHeight * this.scaleY - this.leftAxisModel.lastHeight
    this.leftAxisModel.translate(moveYSource + this.moveY - this.currentYSpace);
    this.setXPosition(this.moveX - this.currentXSpace)

    this.drawChart()
    this.drawHighLight()
  }

  /**
   * 需要覆写的平移事件 的缩放事件
   * @param event
   */
  public onScale(event: TouchEvent) {
    this.onDoubleClick()
  }

  public onSingleClick(event: ClickEvent) {
    let clickX = event.screenX - this.xExtraOffset - this.data.mDisplayRect.left;
    let clickY = this.data.mDisplayRect.bottom - (event.screenY - this.yExtraOffset); //-this.data.mDisplayRect.top;
    if (clickX < 0 || clickY < 0 || clickX > this.data.mDisplayRect.right || clickY > this.data.mDisplayRect.bottom) {
      this.indexHighLightPaint = null;
      return
    }
    if (this.indexHighLightPaint == null || this.indexHighLightPaint == undefined) {
      this.indexHighLightPaint = new IndexPositionPaint();
    }

    clickX = clickX / this.xScale;
    clickY = clickY / this.yScale;
    let dataSetsArray = this.data.getDataSets()
    if (dataSetsArray) {
      for (let i = 0;i < dataSetsArray.length(); i++) {
        let dataSet = dataSetsArray.get(i)

        if (dataSet.isVisible()) {
          for (let j = 0; j < dataSet.getEntryCount(); j++) {
            let entryObj = dataSet.getEntryForIndex(j);
            if (entryObj) {
              let bubbleEntry: EntryOhos | null = dataSet.getEntryForIndex(j) as EntryOhos;
              if (bubbleEntry) {
                let x: number = bubbleEntry.getX();
                let y: number = bubbleEntry.getY();
                if (this.indexHighLightPaint.x == null || this.indexHighLightPaint.y == null) {
                  this.indexHighLightPaint.x = x
                  this.indexHighLightPaint.y = y
                  this.indexHighLightPaint.dataSetIndex = j
                  this.indexHighLightPaint.dataIndex = i
                  continue
                }
                let spaceOldX = Math.abs(this.indexHighLightPaint.x - clickX)
                let spaceOldY = Math.abs(this.indexHighLightPaint.y - clickY)
                let spaceNewX = Math.abs(x - clickX)
                let spaceNewY = Math.abs(y - clickY)
                if (spaceOldX > spaceNewX || (spaceOldX == spaceNewX && spaceOldY > spaceNewY)) {
                  this.indexHighLightPaint.x = x
                  this.indexHighLightPaint.y = y
                  this.indexHighLightPaint.dataSetIndex = j
                  this.indexHighLightPaint.dataIndex = i
                }
              }
            }
          }
        }
      }
    }
    this.drawHighLight()
  }
}